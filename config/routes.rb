Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :readers do
    collection do
      post :search_my_books
    end
  end
  devise_for :users
  root "books#index"
  resources :books do
    resources :book_orders, only:[:new, :show, :create]
  end
  resources :genres
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
