class CreateBookOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :book_orders do |t|
      t.references :reader, foreign_key: true
      t.references :book, foreign_key: true
      t.datetime :return_date

      t.timestamps
    end
  end
end
