class CreateReaders < ActiveRecord::Migration[5.2]
  def change
    create_table :readers do |t|
      t.string :full_name
      t.string :address
      t.string :passport_number
      t.string :read_ticket

      t.timestamps
    end
  end
end
