require 'faker'

AdminUser.create!(email: 'admin@example.com', password: 'password', password_confirmation: 'password') if Rails.env.development?

10.times do
  Genre.create(
           name: Faker::Name.unique.name
  )
end

13.times do |i|
    book = Book.new(title: Faker::Book.title, author: Faker::Name.name, genre_id: "#{rand(1..10)}", availability: [true, false].sample)
    path = Rails.root.join('app', 'assets', 'images', 'fixtures', "#{i}.jpg")
    book.picture.attach(io: File.open(path), filename: "#{i}.jpg")
    book.save
end

10.times do
  Reader.create(
      full_name: Faker::Name.name,
      address: Faker::Address.street_name,
      passport_number: Faker::IDNumber.valid
  )
end

BookOrder.create(
      reader_id: 1,
      book_id: 4,
      return_date: '2019-11-24 16:46:00'
  )

BookOrder.create(
      reader_id: 2,
      book_id: 3,
      return_date: '2019-11-20 16:46:00'
  )

BookOrder.create(
    reader_id: 3,
    book_id: 5,
    return_date: '2019-11-23 16:46:00'
)