class Reader < ApplicationRecord
  before_save :generate_read_ticket
  has_many :book_orders

  validates :passport_number, uniqueness: true

  def generate_read_ticket
    self.read_ticket = Digest::SHA1.hexdigest self.passport_number
  end
end
