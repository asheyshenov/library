class Book < ApplicationRecord
  belongs_to :genre
  has_one_attached :picture
  has_many :book_orders
  validates :title, presence: true, length: { minimum: 3, maximum: 250 }
  validates :author, presence: true, length: { minimum: 3, maximum: 250 }
end
