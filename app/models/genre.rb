class Genre < ApplicationRecord
  has_many :books, :dependent => :destroy

  validates :name, presence: true, length: { maximum: 250 }
end
