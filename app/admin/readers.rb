ActiveAdmin.register Reader do

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :full_name, :address, :passport_number, :read_ticket
  #
  # or
  #
  # permit_params do
  #   permitted = [:full_name, :address, :passport_number, :read_ticket]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
