ActiveAdmin.register Book do

  permit_params :title, :author, :genre_id, :picture, :availability

end
