class ReadersController < ApplicationController
  before_action :set_reader, only: [:show, :edit, :update, :destroy]

  def index
    @reader = Reader.all()
  end

  def new
    @reader = Reader.new()
  end

  def create
    @reader = Reader.new(reader_params)

    respond_to do |format|
      if @reader.save
        format.html { redirect_to @reader, notice: 'Reader was successfully created.' }
        format.json { render :show, status: :created, location: @reader }
      else
        format.html { render :new }
        format.json { render json: @reader.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  def search_my_books
    if params[:user][:read_ticket].present?
      @reader = Reader.find_by(read_ticket: params[:user][:read_ticket])
      @book_orders = @reader.book_orders
      if @reader.present?
        render 'index'
      else
        flash[:notice] = "Enter the correct data"
        render 'index'
      end
    else
      redirect_back(fallback_location: root_path)
    end
  end


  private

  def set_reader
    @reader = Reader.find(params[:id])
  end


  def reader_params
    params.require(:reader).permit(:full_name, :address, :passport_number)
  end
end
