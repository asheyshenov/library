class BookOrdersController < ApplicationController

  before_action :set_book_order, only: [:show, :edit, :update, :destroy]
  before_action :set_book, only: [:new, :create]

  def new
    @book_order = BookOrder.new
  end

  def create
    @reader = Reader.find_by(read_ticket: params[:book_order][:reader])
    @book_order = BookOrder.new(book_order_params)
    @book_order.reader = @reader
    @book_order.book = @book
      if @book_order.save
        @book.update_attribute(:availability, false)
        redirect_to root_path, notice: 'Book order was successfully created.'
      else
        render :new
      end
    end
  end

  private

  def set_book_order
    @book_order = BookOrder.find(params[:id])
  end

  def set_book
    @book = Book.find(params[:book_id])
  end

  def book_order_params
    params.require(:book_order).permit(:return_date)
  end

