class BooksController < ApplicationController
  def index
    @books  = Book.all()
  end

  def new
    @book = Book.new()
  end

  def show
    @book = Book.find(params[:id])
  end

  def create
    @book = Book.new(book_params)
    if @book.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
    @book = Book.find(params[:id])
  end

  def upload_picture
    @book.picture.attach(uploaded_file) if uploaded_file.present?
  end

  def uploaded_file
    params[:book][:picture]
  end

  def update
    @book = Book.find(params[:id])
    if @book.update(book_params)
      redirect_to @book
    else
      render 'edit'
    end
  end


  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    redirect_to root_path
  end

  private

  def book_params
    params.require(:book).permit(:title, :author, :genre_id, :picture, :availability)
  end
end
