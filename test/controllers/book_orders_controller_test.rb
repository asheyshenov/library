require 'test_helper'

class BookOrdersControllerTest < ActionDispatch::IntegrationTest
  test "should get new" do
    get book_orders_new_url
    assert_response :success
  end

  test "should get create" do
    get book_orders_create_url
    assert_response :success
  end

end
